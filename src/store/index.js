import Vue from "vue";
import Vuex from "vuex";
import VueCookies from 'vue-cookies'
import axios from "axios";

Vue.use(Vuex);
Vue.use(VueCookies)

axios.defaults.baseURL = "https://todolist-tvn-demo.herokuapp.com/";

export default new Vuex.Store({
  state: {
    loading: false,
    loggedIn: localStorage.getItem('access_token') !== null ? true : false,
    token: localStorage.getItem('access_token') || null,
    userId: localStorage.getItem('user_id') || null,
    username: localStorage.getItem('user_name') || null,
    error: null,
    todos: [],
    todoPagination: [],
    filter: 'all',
    rows: null,
    pagePagination: {
      perPage: 5,
      currentPage: 1,
      rows: 20
    }
  },
  getters: {
    getFilter(state) {
      return state.filter;
    },
    getPerPage(state) {
      return state.pagePagination.perPage;
    },
    getCurrentPage(state) {
      return state.pagePagination.currentPage;
    },
    getRows(state) {
      return state.rows;
    },
    users(state) {
      return state.users;
    },
    todos(state) {
      return state.todos;
    },
    numAllTodos(state) {
      return state.todos.length;
    },
    numTodosCompleted(state) {
      return state.todos.filter(todo => todo.completed).length;
    },
    lengthTodos(state) {
      return state.todos.length;
    },
    getError(state) {
      return state.error;
    }
  },
  mutations: {
    setLoading(state, payload) {
      state.loading = payload
    },
    setCompleted(state, idTodo) {
      const todoOrigin = state.todos
        .filter(todo => todo.id === idTodo)
        .map(todo => {
          return todo.completed = !todo.completed;
        });

      const todoTemp = state.todoPagination
        .filter(todoTemp => todoTemp.id === idTodo)
        .map(todoTemp => {
          return todoTemp.completed = todoOrigin[0];
        });
    },
    deleteTodo(state, idTodo) {
      const indexTodo = state.todos.findIndex(todo => todo.id == idTodo);
      const indexTodoPagination = state.todoPagination.findIndex(
        todo => todo.id == idTodo
      );
      state.todos.splice(indexTodo, 1);
      state.todoPagination.splice(indexTodoPagination, 1);
    },
    addTodo(state, todo) {
      state.todos.push({
        user_id: todo.userId,
        id: todo.id,
        title: todo.title,
        completed: todo.completed
      });
      state.todoPagination.push({
        user_id: todo.userId,
        id: todo.id,
        title: todo.title,
        completed: todo.completed
      });
    },
    updateTodo(state, payload) {
      const t3 = state.todos
        .filter(todo => todo.id === payload.id)
        .map(todo => {
          return todo.title = payload.titleUpdate;
        });
      state.todoPagination
        .filter(todoTemp => todoTemp.id === payload.id)
        .map(todoTemp => {
          return todoTemp.title = payload.titleUpdate;
        });
    },
    deleteAllTodo(state) {
      state.todos
        .filter(todo => todo.completed === true)
        .forEach(todoCompleted => {
          const index = state.todos.findIndex(
            todo => todo.id == todoCompleted.id
          );
          state.todos.splice(index, 1);
        });
      state.todoPagination
        .filter(todo => todo.completed === true)
        .forEach(todoCompleted => {
          const index = state.todoPagination.findIndex(
            todo => todo.id == todoCompleted.id
          );
          state.todoPagination.splice(index, 1);
        });
    },
    retrieveTodos(state, todos) {
      if (todos == null) {
        state.todos = [];
        state.todoPagination = [];
      }
      else {
        state.todos = todos;
        state.rows = todos.length;
        state.todoPagination = state.todos.slice(0, state.pagePagination.perPage);
      }

    },
    getDataTodoPagination(state, pagination) {
      const filterBy = pagination.filter

      if (filterBy === "finished") {
        var todosFilter = state.todos.filter(todo => todo.completed)
      }
      else if (filterBy === "unfinished") {
        var todosFilter = state.todos.filter(todo => !todo.completed)
      }
      else {
        var todosFilter = state.todos;
      }

      const start = (pagination.currentPage - 1) * pagination.perPage;
      const end = pagination.currentPage * pagination.perPage;

      state.todoPagination = todosFilter.slice(start, end);
      state.pagePagination.currentPage = pagination.currentPage;
      state.pagePagination.perPage = pagination.perPage;
      state.rows = todosFilter.length;
    },

    getDataTodoByFilter(state, payload) {
      state.filter = payload.filter;
      const perPage = state.pagePagination.perPage;
      const currentPage = payload.currentPage;

      const filterBy = payload.filter
      if (filterBy === "finished") {
        var todosFilter = state.todos.filter(todo => todo.completed)
      }
      else if (filterBy === "unfinished") {
        var todosFilter = state.todos.filter(todo => !todo.completed)
      }
      else {
        var todosFilter = state.todos;
      }

      const start = (currentPage - 1) * perPage;
      const end = currentPage * perPage;
      state.todoPagination = todosFilter.slice(start, end);
      state.pagePagination.currentPage = currentPage;
      state.pagePagination.perPage = perPage;
      state.rows = todosFilter.length;
    },

    login(state, payload) {
      state.loggedIn = true;
      state.token = payload.token;
      state.userId = payload.userId;
      state.username = payload.username;
      // $cookies.set('access_token', payload.token, 60 * 60 * 12);
      // $cookies.set('user_id', payload.userId, 60 * 60 * 12);
      // $cookies.set('user_name', payload.username, 60 * 60 * 12);
      console.log(localStorage);

      localStorage.setItem('access_token', payload.token)
      localStorage.setItem('user_id', payload.userId)
      localStorage.setItem('user_name', payload.username)

      // console.log(localStorage.getItem('access_token') );

    },
    destroyToken(state) {
      state.loggedIn = false;
      state.token = null;
      state.userId = null;
      state.username = null;
      //   $cookies.remove('access_token');
      // $cookies.remove('user_id');
      // $cookies.remove('user_name');
      // this.$router.push("/login");
      // localStorage.removeItem('access_token'),
      //   localStorage.removeItem('user_id'),
      //   localStorage.removeItem('user_name')
      localStorage.clear();
    },
    setError(state, message) {
      state.error = message
    },
    changeId(state, payload) {
      state.todos
        .filter(todo => todo.id === payload.idOld)
        .map(todo => {
          return todo.id = payload.idNew;
        });
      state.todoPagination
        .filter(todoTemp => todoTemp.id === payload.idOld)
        .map(todoTemp => {
          return todoTemp.id = payload.idNew;
        });
    }
  },

  actions: {
    doneTodo({ commit, state }, payload) {
      commit("setCompleted", payload.idTodo);
      const userId = state.userId;
      axios({
        method: 'put',
        url: "/todos/" + userId + "/" + payload.idTodo,
        withCredentials: true,
        data: {
          completed: !payload.completed,
        }
      })
        .then(function (response) {
          console.log(response.data.result + " - " + response.data.message);
        })
        .catch(error => {
          console.log(error);
        });
    },

    deleteTodo({ commit, state }, todoId) {
      commit("deleteTodo", todoId);
      console.log("todo id trong action");
      const userId = state.userId;
      axios({
        method: 'delete',
        url: "/todos/" + userId + "/" + todoId,
        withCredentials: true
      })
        .then(function (response) {
          console.log(response.data.result + " - " + response.data.message);
        })
        .catch(error => {
          console.log(error);
        });
    },

    deleteAllTodo({ commit, state }, payload) {
      const arrTodoCompleted = state.todos
        .filter(todo => todo.completed === true).map(todo => todo.id);
      commit("deleteAllTodo");
      const userId = state.userId;
      axios({
        method: 'post',
        url: "/todos/all/" + userId,
        withCredentials: true,
        data: {
          arrTodoCompleted: arrTodoCompleted
        }

      })
        .then(function (response) {
          console.log(response.data.result + " - " + response.data.message);
        })
        .catch(error => {
          console.log(error);
        });
    },

    addTodo({ commit, state }, payload) {
      const userId = state.userId;
      console.log(payload);
      commit("addTodo", payload);
      axios({
        method: 'post',
        url: "/todos/" + userId,
        withCredentials: true,
        data: {
          title: payload.title,
          completed: payload.completed,
          created_on: payload.created_on
        }
      })
        .then(function (response) {
          commit("changeId", { idNew: response.data.data.id, idOld: payload.id });
        })
        .catch(error => {
          console.log(error);

        });
    },

    updateTodo({ commit, state }, payload) {
      commit("updateTodo", payload);
      const userId = state.userId;
      axios({
        method: 'put',
        url: "/todos/" + userId + "/" + payload.id,
        withCredentials: true,
        data: {
          title: payload.titleUpdate,
        }
      })
        .then(function (response) {
          console.log(response.data.result + " - " + response.data.message);
        })
        .catch(error => {
          console.log(error);

        });
    },

    retrieveTodos({ state, commit }) {
      commit("setLoading", true);
      const userId = state.userId;
      axios.defaults.headers.common["Authorization"] = localStorage.getItem('access_token');
      // axios.defaults.withCredentials = true
      axios
        .get("/todos/userid/" + userId, { withCredentials: true })
        .then(response => {
          var todos = response.data.result === "Failed" ? null : response.data.data;
          commit("retrieveTodos", todos);
          commit("setLoading", false);
        })
        .catch(error => {
          console.log(error);
          commit("setLoading", false);
        });
    },

    changeFilter({ commit }, payload) {
      commit("getDataTodoByFilter", payload);
    },

    ChangeCurrentPage({ commit }, payload) {
      commit("getDataTodoPagination", payload);
    },

    loginAction({ commit }, payload) {
      commit("setLoading", true);
      axios({
        method: 'post',
        url: "/auth/login",
        data: {
          email: payload.email,
          password: payload.password
        }
      })
        .then(function (response) {
          const token = response.data.token;
          const userId = response.data.userId;
          const username = response.data.username;

          commit("login", { token, userId, username });
          commit("setError", null);
          commit("setLoading", false);
        })
        .catch(error => {
          commit("setError", error.response.data.message);
          commit("setLoading", false);
        });
    },

    registerAction({ commit }, payload) {
      commit("setLoading", true);
      return new Promise((resolve, reject) => {
        axios({
          method: 'post',
          url: "/auth/register",
          data: {
            email: payload.email,
            password: payload.password,
            username: payload.username,
            created_on: payload.created_on,
            last_login: payload.last_login
          }
        }).then(function (response) {
          console.log(response);
          console.log(response.data.data.email + response.data.data.password);
          const user = {
            email: response.data.data.email,
            password: response.data.data.password
          }
          resolve(user)
        })
          .catch(error => {
            commit("setError", error.response.data.message);
            commit("setLoading", false);
            reject(error)

          });
      })
    },

    logoutAction({ commit, getters, state }) {
      commit('destroyToken')
    },
  }
});
